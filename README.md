# repositoryFromVS
Работа с Git

Создание репозитория:
    1) Создаем папку проекта
   2) Переходим в нее в git bash, например: > d:/gitrepos/repo1
   3) Делаем из нее репозиторий через команду git init
   4) git config --global user.name “[name]”  

   git config --global user.email “[email address]”  
      Введите команду followong с действующим адресом электронной почты, который вы используете для github ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
   5) Создаем репозиторий на GitHub, копируем его SSH или HTTP
   6) 
   7) Связываем репозиторий через команду git remote add origin %SSH-протокол%
   8) Проверка: git remote -v
   9) git remote set-url origin git@github.com:<url-repo>.git
   10) ssh -T git@github.com
   12) В дальнейшем добавляем изменения так: git status → git add . → git commit -a → git push
   13) Первый коммит: git push --set-upstream origin master

Стянуть существующий репозиторий:
    git clone %http-address% %destintation folder%

Переход на другую ветку:
    git checkout %branch name%
